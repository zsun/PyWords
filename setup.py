#!/usr/bin/env python3

import os
import configparser

from setuptools import setup, find_packages


VERSION = '0.0.1'

setup(
    name='PyWords', 
    version=VERSION, 
    description='building a powerful vocabulary', 
    author='Casey', 
    author_email='caseyzsun@gmail.com', 
    url='https://github.com/caseyzsun/PyWords', 
    license='MIT', 
    packages=find_packages(), 
    python_requires='~=3.5', 
    install_requires=['click', 'numpy'], 
    entry_points={
        'console_scripts': [
            'pywords=pywords.run:main'
            ]
    }
)

conf = configparser.ConfigParser()
path = os.path.join(os.environ['HOME'], '.wrc')

if not os.path.exists(path):
    app_id = input('your Youdao appKey: ')
    app_key = input('your Youdao secretKey: ')

    conf['Youdao'] = {'app_id': app_id, 'app_key': app_key}

    with open(path, 'w') as f:
        conf.write(f)
