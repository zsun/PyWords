# -*- coding: utf-8 -*-
"""
Created on Mon Apr 24 2017

@author: Casey
"""

import os
import configparser
import pprint

import click

from pywords import api


conf = configparser.ConfigParser()
conf.read(os.environ['HOME']+'/.wrc')
app_id = conf['Youdao']['app_id']
app_key = conf['Youdao']['app_key']


@click.command()
@click.argument('word', default='love')
def main(word):

    data = eval(api.youdao(word, app_id, app_key))

    pp = pprint.PrettyPrinter()
    pp.pprint(data)


if __name__ == '__main__':
    main()
