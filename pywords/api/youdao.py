# -*- coding: utf-8 -*-
"""
Created on Mon Apr 24 2017

@author: Casey
"""

import random
import http

from urllib.request import hashlib
from urllib.parse import quote
from urllib.parse import urlencode


def youdao(q, app_id, app_key):

    http_client = None
    myurl = '/api'

    salt = random.randint(1, 65536)
    sign = app_id + q + str(salt) + app_key
    m1 = hashlib.md5()
    m1.update(sign.encode())
    sign = m1.hexdigest()

    d = {'appKey': app_id,
         'q': quote(q),
         'from': 'auto',
         'to': 'auto',
         'salt': str(salt),
         'sign': sign
         }

    myurl += '?' + urlencode(d)

    try:
        http_client = http.client.HTTPConnection('openapi.youdao.com')
        http_client.request('GET', myurl)

        response = http_client.getresponse()
        r = response.read().decode()

    except Exception as e:
        r = e

    finally:
        if http_client:
            http_client.close()

    return r


if __name__ == '__main__':
    print(__doc__)